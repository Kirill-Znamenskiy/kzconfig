package kzconfig

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/joho/godotenv"
	"github.com/octago/sflags/gen/gpflag"
	"github.com/sethvargo/go-envconfig"
	"github.com/spf13/pflag"
)

// LoadConfigFromEnv load config struct from env and/or env-files.
func LoadConfigFromEnv(ctx context.Context, cfg any, envFilePaths ...string) (err error) {
	dotEnvMap := make(map[string]string)
	if len(envFilePaths) > 0 {
		for _, envFilePath := range envFilePaths {
			envFile, err := os.Open(envFilePath)
			if err != nil {
				if errors.Is(err, os.ErrNotExist) {
					continue
				}
				return err
			}

			envFileMap, err := godotenv.Parse(envFile)
			if err != nil {
				return err
			}

			for key, value := range envFileMap {
				dotEnvMap[key] = value
			}

			err = envFile.Close()
			if err != nil {
				return err
			}
		}
	}

	return envconfig.ProcessWith(ctx, cfg, newMyCustomLookuper(dotEnvMap))
}

// ParseConfigFlags parse config flags.
func ParseConfigFlags(cfg any) (err error) {

	fs := pflag.NewFlagSet(os.Args[0], pflag.ContinueOnError)

	err = gpflag.ParseTo(cfg, fs)
	if err != nil {
		return err
	}

	err = fs.Parse(os.Args[1:])
	if err != nil {
		if errors.Is(err, pflag.ErrHelp) {
			fmt.Println("Help message finished.")
			os.Exit(0)
		}
		return err
	}

	return err
}

type myCustomLookuper struct {
	dotEnvMap map[string]string
}

func newMyCustomLookuper(dotEnvMap map[string]string) *myCustomLookuper {
	return &myCustomLookuper{dotEnvMap: dotEnvMap}
}

// Lookup value by key.
func (mcl *myCustomLookuper) Lookup(key string) (val string, ok bool) {
	for ind := 1; ind <= 2; ind++ {
		if ind == 1 {
			//val, ok = envconfig.OsLookuper().Lookup(key)
			val, ok = os.LookupEnv(key)
		} else if ind == 2 && mcl.dotEnvMap != nil {
			//val, ok = envconfig.MapLookuper(mcl.dotEnvMap).Lookup(key)
			val, ok = mcl.dotEnvMap[key]
		} else {
			val, ok = "", false
		}

		if !ok {
			continue
		}

		// if env variable exists, but empty string - ignore it
		if ok && val == "" {
			ok = false
		}

		if !ok {
			continue
		}

		// if it consists only from empty spaces or/and quotes - ignore it too
		if ok {
			tmp := val
			tmp = strings.TrimSpace(tmp)
			tmp = strings.Trim(tmp, " '\"\t\n\v\f\r")
			tmp = strings.TrimSpace(tmp)
			if tmp == "" {
				ok = false
				val = ""
			}
		}

		if !ok {
			continue
		}

		return val, ok
	}

	return val, ok
}

// ConvertConfigToPrettyString convert config to pretty string.
func ConvertConfigToPrettyString(cfg any) string {
	bts, err := json.MarshalIndent(cfg, "", "    ")
	if err != nil {
		log.Fatal(err)
		return ""
	}
	return string(bts)
}

// GetEnvValue get env value or default value.
func GetEnvValue(key string, defaultValue string) (ret string) {
	ret = os.Getenv(key)
	if ret == "" {
		ret = defaultValue
	}
	return ret
}
