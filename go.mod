module gitlab.com/Kirill-Znamenskiy/kzconfig

go 1.19

require (
	github.com/spf13/pflag v1.0.3
	github.com/joho/godotenv v1.5.1
	github.com/sethvargo/go-envconfig v0.9.0
	github.com/octago/sflags v0.3.1-0.20210726012706-20f2a9c31dfc
)
